import numpy as np

# 4x + 5y = 20
# x + 2y = 13

# 係数（xとyにかけられている値）
coefficients = np.array([[4, 5], [1, 2]])

# 方程式の右辺
dependents = np.array([20, 13])

# そしてlinalgモジュールのsolve()関数を使う
answers = np.linalg.solve(coefficients, dependents)

print(answers)  # [ -8.33333333  10.66666667]

print(4 * answers[0] + 5 * answers[1])  # 20
print(answers[0] + 2 * answers[1])  # 13

product = np.dot(coefficients, answers)  # [20. 13.]
print(product)

# 二つの配列がほぼ等しいか調べる
print(np.allclose(product, dependents))


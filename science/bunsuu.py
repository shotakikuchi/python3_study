from fractions import Fraction
from fractions import gcd
import numpy as np

print(Fraction(1, 3) * Fraction(2, 3))

print(gcd(24, 18))

b = np.array([2, 4, 6, 8])
print(b)

#階数を表す
print(b.ndim)

#配列内の値の総数を返す
print(b.size)

#各階の値の数は、shapeから得られる
print(b.size)


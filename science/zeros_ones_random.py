import numpy as np

a = np.zeros(3, )
print(a)

# 階数が2になる
b = np.zeros((2, 4))
print(b)

# すべて1で配列を生成する
k = np.ones((3, 5))
print(k)

# random()は0.0から1.0までの無作為な値を使って配列を作る
m = np.random.random((3, 4))
for mm in m:
  for mmm in mm:
    print(mmm * 10)


import numpy as np

a = np.arange(10)

a = a.reshape(2, 5)

print(a)

# 上記と同じ結果が得られる
b = np.arange(10)
b.shape = (2, 5)
print(b)


import numpy as np

b = np.array([2, 3, 4, 5])

# 階数を返す
print(b.ndim)

# 配列内の値の総数
print(b.size)

# 各階の値の数
print(b.shape)

c = np.arange(10)

print(c)
print(c.shape)
print(c.size)

# 2つの値を渡すと、最初の値から第2の値-1までの配列が得られる。
a = np.arange(7, 10)

print(a)

# 第3引数として1以外のステップを指定できる
d = np.arange(5, 15, 2)
print(d)

# floatでもきちんと動作する
f = np.arange(2.0, 9.8, 0.3)
print(f)

# dtype引数を指定すると、どの型の値を生成するかをarangeに指示できる
g = np.arange(10, 4, -1.5, dtype=np.float)
print(g)
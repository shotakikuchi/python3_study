from numpy import *

a = arange(4)
print(a)

a *= 3
print(a)

# Python標準のリストの各要素同じ値をかけようとしたら、ループかリスト内包表記を使わなければいけない。
plain_list = list(arange(4))

plain_list = [num * 3 for num in plain_list]
print(plain_list)

c = zeros((2, 5)) + 17.0
print(c)

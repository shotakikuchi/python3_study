class A():
  count = 0

  def __init__(self):
    A.count += 1

  def exclaim(self):
    print("Im an A")

  @classmethod
  def kids(cls):
    print("A has", cls.count, "little Objects.")


easy_a = A()

breeasy_a = A()

wheezy_a = A()

A.kids()

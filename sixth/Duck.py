# class Duck():
#     def __init__(self, input_name):
#         self.hidden_name = input_name
#
#     def get_name(self):
#         print("inside getter")
#         return self.hidden_name
#
#     def set_name(self, input_name):
#         print("indide setter")
#         self.hidden_name = input_name
#
#     name = property(get_name,set_name)


class Duck():
    def __init__(self, input_name):
        self.hidden_name = input_name

    @property
    def name(self):
        print("inside getter")
        return self.hidden_name

    @name.setter
    def name(self, input_name):
        print("indide setter")
        self.hidden_name = input_name



fowl = Duck("Howard")

print(fowl.name)
fowl.name = "Daffy"



class Laser():
  def does(self):
    return "disintegrate"


class Claw():
  def does(self):
    return "crush"


class SmartPhone():
  def does(self):
    return "ring"


class Robot():
  def __init__(self, laser, claw, smartpthon):
    self.laser = laser
    self.claw = claw
    self.smartphone = smartpthon

  def does(self):
    print(
      self.laser.does(),
      self.claw.does(),
      self.smartphone.does())


robot = Robot(Laser(), Claw(), SmartPhone())

robot.does()

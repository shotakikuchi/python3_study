class Duck():
  def __init__(self, input_name):
    self.__name = input_name

  @property
  def name(self):
    return self.__name

  @name.setter
  def name(self, input_name):
    self.__name = input_name


fowl = Duck("Howard")
print(fowl.name)

fowl.name = "fafa"
print(fowl.name)

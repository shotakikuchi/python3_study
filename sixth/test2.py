class Bear():
  def eats(self):
    print("berries(Bear)")


class Rabbit():
  def eats(self):
    print("clover(Rabbit)")


class Octothorpe():
  def eats(self):
    print("campers(Octothorpe)")


bear = Bear()
rabbit = Rabbit()
octothorpe = Octothorpe()

bear.eats()
rabbit.eats()
octothorpe.eats()

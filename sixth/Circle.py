class Circle():
    def __init__(self, input_radius):
        self.radius = input_radius

    @property
    def diameter(self):
        return 2 * self.radius


c = Circle(5)

print(c.radius)

print(c.diameter)


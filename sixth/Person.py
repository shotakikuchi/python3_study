class Person():
    def __init__(self, name):
        self.name = name


class MDPerson(Person):
    def __init__(self, name):
        self.name = "Doctor " + name


class JDPerson(Person):
    def __init__(self, name):
        self.name = name + ", Esquire"


class EmailPerson(Person):
    def __init__(self, name, email):
        super().__init__(name)
        self.email = email


person = Person("Fudd")
doctor = MDPerson("Fudd")
lawyer = JDPerson("Fudd")

bob = EmailPerson("bob","good@gmail.com")

print(person.name)
print(doctor.name)
print(lawyer.name)
print(bob.email)

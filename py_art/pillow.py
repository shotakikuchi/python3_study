from PIL import Image

img = Image.open('oreilly.jpg')

# confirm ext
print('ext', img.format)

# confirm size
print('size', img.size)

# confirm mode
print('mode', img.mode)

# show img
# img.show()

# crop = (90, 90, 120, 120)
# img2 = img.crop(crop)
# img2.show()

# save img
# img2.save('cropped.gif', 'GIF')
#
# img3 = Image.open('cropped.gif')
# print(img3.format)
#
# print(img3.size)

mustache = Image.open('moustaches.jpeg')
handler = mustache.crop((10, 20, 490, 182))

#handler.show()

img.paste(handler, (45, 90))

img.show()

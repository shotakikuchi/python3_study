years_list = [1993,1994,1995,1996,1997,1998]

print(years_list[3])

print(years_list[-1])

things = ["mozzarella","cinderrella","salmonella"]

things[1] = things[1].title()

print(things[1])

things[2] = things[2].title()

del things[2]

print(things)

surprise = ["Groucho","Chico","Harpo"]

surprise[-1] = surprise[-1].lower()

print(surprise[-1])

surprise.sort(reverse=True)

surprise[0] = surprise[0].title()

print(surprise[0])

e2f = {
    "dog": "chien",
    "cat": "chat",
    "walrus": "morse"
}

print(e2f["walrus"])

f2e = {}

for english,french in e2f.items():
    f2e[french] = english

print(f2e)

print(f2e["chien"])

dic = set(e2f.keys())

print(dic)

life = {
    "animals":{"cats":["Henri","Grumpy","Lucy",],"octopi":"","emus":""},
    "plants":"",
    "other":"",
}

print(life.keys())

print(life["animals"].keys())
print(life["animals"]["cats"])
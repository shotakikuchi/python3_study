pythons = {
    "Champman": "Graham",
    "Cleese": "John",
}

print("Champman" in pythons)

print(pythons.get("Cleese"))

print(pythons.get("fafa","Not a python"))

print(pythons.get("fafa"))

print(pythons.keys())


print(list(pythons.values()))

print(list(pythons.items()))

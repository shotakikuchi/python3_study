empty_list = []

print(list)

print(list("cat"))

marxes = ["Groucho","Chico","Harpo"]

print(marxes[0:2])

print(marxes[::-1])

others = ["Gummo", "Karl"]

print(marxes.extend(others))

marxes += others

print(marxes)

del marxes[2]

print(marxes)

print("Karl" in marxes)

print(marxes.count("Groucho"))

numbers = [1,4.0,2,3]

sort_num = sorted(numbers)

print(sort_num)

numbers.sort(reverse=True)

print(numbers)


pythons = {
    "Champman": "Graham",
    "Cleese": "John",
}

pythons["Gilliam"] = "Gerry"

print(pythons)

others = {
    "Marx": "Groucho",
    "Howard": "Moe",
}

pythons.update(others)

print(pythons)

del pythons["Howard"]

print(pythons)

pythons.clear()

print(pythons)


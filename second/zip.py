days = ["Monday", "Tuesday", "Wednesday"]
fruits = ["banana", "apple", "orange"]
drinks = ["coffee", "milk", "tea", "coke"]
desserts = ["tiramisu", "ice cream", "pie", "pudding"]

for day, fruit, drink, dessert in zip(days, fruits, drinks, desserts):
  print(day, ": drink", drink, "- fruit", fruit, "- dessert", dessert)

english = "Monday", "Tuesday", "Wednesday"
french = "Lundi", "Mardi", "Mercredi"

print(dict(list(zip(english, french))))

print(range(0, 11, 2))

print(range(10, -1, -1))

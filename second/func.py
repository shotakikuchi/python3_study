def run_something(func):
    func()


def answer():
    print(42)


run_something(answer)


def sum_args(*args):
    return sum(args)


def run_with_positional_args(func, *args):
    return func(*args)


result = run_with_positional_args(sum_args, 1, 2, 3, 4)

print(result)
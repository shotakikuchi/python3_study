def do_nothing():
    pass


do_nothing()


def make_a_sound():
    print("quack")


make_a_sound()


def agree():
    return True


if agree():
    print("Splendid")
else:
    print("That was unexpected")


def echo(anything):
    print(anything + ' ' + anything)


echo("String")


def commentary(color):
    if color == "red":
        return "It's a tomato"
    elif color == "green":
        return "It's a green pepper"
    elif color == "bee purple":
        return "I don't know what it is, but only bees can see it"


comment = commentary("bee purple")

print(comment)


def print_args(*args):
    print("Positional argument tuple:", args)


print_args(1, 2, 3, "wait", "uh..")


def print_more(required1, required2, *args):
    print("Need this one:", required1)
    print("Need this one too:", required2)
    print("All the rest:", args)


print_more("an", "un", "fafa", "fafa", 2)


def print_kwrags(**kwargs):
    print("Keyword arguments:", kwargs)


print_kwrags(wine="merlot", entree="mutton")

def print_if_true(thing,check):
    '''
    :param thing: num
    :param check: boolean
    :return:thing if check == True
    '''

    if check == True:
        print(thing)


help(print_if_true)

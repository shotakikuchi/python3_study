rabbits = ["Flospy", "Mopsy", "Cotontail", "Peter"]

for rabbit in rabbits:
  print(rabbit)

word = "abc"
for letter in word:
  print(letter)

accusation = {
  "room": "ballroom", "weapon": "lead pipe", "person": "Col Mustard"
}

for card in accusation.keys():
  print(card)

for value in accusation.values():
  print(value)

number_lists = list(range(1,6))

print(number_lists)

number_lists1 = [number for number in range(1,6)]

print(number_lists1)

a_list = [number for number in range(1,8) if number % 2 == 1]

print(a_list)

rows = range(1,4)
cols = range(1,3)

cells = [(row,col) for row in rows for col in cols]

for cell in cells:
    print cell

word = "letters"

letter_counts = {letter: word.count(letter) for letter in set(word)}

print(letter_counts)
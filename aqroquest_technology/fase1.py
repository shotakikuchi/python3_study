# -*- coding: utf-8 -*-
ANSWER = '128'
counter = 1


def get_template_text(counter):
  return (
    """
    Great&Goodを開始します！
    %d回目のチャレンジ！
    3桁の数値を入力してください:""" % (counter))


while True:
  input_num = input(get_template_text(counter))

  if input_num == ANSWER:
    print("正解です！！")
    break

  print("残念、違います。")
  counter += 1

  if counter == 11:
    break

# -*- coding: utf-8 -*-
ANSWER = '128'
counter = 1
answers_arr = [nums for nums in ANSWER]


def get_template_text(counter):
  return (
    """
Great&Goodを開始します！
%d回目のチャレンジ！
3桁の数値を入力してください:""" % (counter))


while True:
  great_counter = 0
  good_counter = 0

  input_num = input(get_template_text(counter))

  if input_num == ANSWER:
    print("正解です！！")
    break

  for number, answers, in enumerate(answers_arr, 0):
    if answers == input_num[number]:
      great_counter += 1
    elif answers in input_num:
      good_counter += 1

  print("Great: ", great_counter)
  print("Good: ", good_counter)
  print("残念、違います。")
  counter += 1

  if counter == 11:
    break

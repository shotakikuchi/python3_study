letters = "abcdefg"

print(len(letters))

todos = "a,b,c,e,f,g"

array = (todos.split(","))

joined = ":".join(array)

print(joined)

poem = """word word word false
fafa fafa fafa faf true
false"""

print(poem.find("false"))

print(len(poem))

print(poem.count("word"))

setup = "a duck goes into a bar ..."

print(setup.strip("."))

print(setup.capitalize())

print(setup.title())

print(setup.upper())

print(setup.swapcase())

print(setup.center(30))

print(setup.ljust(30))

print(setup.rjust(30))

print(setup.replace("duck","marmost"))

seconds_per_hour = 60 * 60
print(seconds_per_hour)

seconds_per_day = seconds_per_hour * 24
print(seconds_per_day)

hour_per_day = seconds_per_day / seconds_per_hour

print(hour_per_day)


hour_per_day = seconds_per_day // seconds_per_hour


print(hour_per_day)



guess_me = 7
start = 1

if guess_me < 7:
    print("too low")
elif guess_me == 7:
    print("just right")
else:
    print("too high")

while start < guess_me:
    if start < guess_me:
        print("too low")

    elif start == guess_me:
        print("fount it!")
    else:
        print("oops")
        break

    start += 1

for num in list(range(4, -1, -1)):
    print (num)

even = list(range(0, 11, 2))

print("enen :", even)

squares = {key: key * key for key in range(0, 11)}

print(squares)

odd = {number for number in range(10) if number % 2 == 1}

print(odd)

for thing in ("Got %s" % number for number in range(10)):
    print(thing)


def good():
    return ["Harry", "Ron", "Hermione"]


print good()


def get_oods():
    for num in range(10):
        if num % 2 == 1:
            yield num


for count, number in enumerate(get_oods(), 1):
    if count == 3:
        print("the third number is", number)


def decorater(func):
    def wrapper_func(*args, **kwargs):
        print("start")
        result = func(*args, **kwargs)
        print("test")
        return result

    return wrapper_func


@decorater
def sub_num(a, b):
    return a + b


print(sub_num(4, 5))

titles = ["Creature of Habit", "Crewel Fate"]
plots = ["A num turns into a monster", "A haunted yarn shop"]

movies = dict(zip(titles, plots, ))

print(movies)


def get_even():
    for num in range(10):
        if num % 2 == 0:
            yield num


for count, number in enumerate(get_even(), 1):
    if count == 3:
        print(number)

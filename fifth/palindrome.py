#スタック + キュー = デック
def palindrome(word):
    from collections import deque
    dq = deque(word)
    while len(dq) > 1:
        if dq.popleft() != dq.pop():
            print("False")
            return False
    print("True")
    return True

palindrome("a")

palindrome(" ")

palindrome('radar')

palindrome("halibut")

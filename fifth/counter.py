from collections import Counter

breakfast = ["spam", "spam", "egg", "spam"]

breakfast_counter = Counter(breakfast)

print(breakfast_counter)

print(breakfast_counter.most_common())
print(breakfast_counter.most_common(1))

lunch = ["egg", "egg", "bacon"]
lunch_counter = Counter(lunch)
print(lunch_counter)

# 二つのカウンタを結合する第一の方法
print(breakfast_counter + lunch_counter)

# 朝食では食べるが昼食では食べないもの
print(breakfast_counter - lunch_counter)

# 昼食では食べるが朝食では食べないもの
print(lunch_counter - breakfast_counter)

print(lunch_counter & breakfast_counter)

print(lunch_counter | breakfast_counter)

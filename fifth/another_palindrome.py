def another_palindrome(word):
    return word == word[::-1]

result1 = another_palindrome("radar")

print(result1)

result2 = another_palindrome("halibut")

print(result2)
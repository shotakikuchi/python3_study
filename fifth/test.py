# from module import zoo as menagerie
from zoo import hours as info
from collections import OrderedDict,defaultdict


info()

plain = {"a": 1, "b": 2, "c": 3}

print(plain)

fancy = OrderedDict([
    ("a", 1), ("b", 2), ("c", 3)
])

print(fancy)

dict_of_lists = defaultdict(list)

dict_of_lists["a"] = "something for a"

print(dict_of_lists)

print(dict_of_lists["b"])
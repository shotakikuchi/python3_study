import itertools

# cycleは無限反復子 引数から循環的に要素を返す。
# for item in itertools.cycle([1, 2]):
#     print(item)

# accumulate()は要素を一つにまとめた値を計算する。デフォルトでは和を計算する。
for item in itertools.accumulate([1, 2, 3, 4]):
    print(item)


# accumulateは第二引数として関数を受付、この引数が加算の代わりに使われる。

def multiply(a, b):
    return a * b


for item in itertools.accumulate([2, 3, 4, 5], multiply):
    print(item)

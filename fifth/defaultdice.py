from collections import defaultdict

pefiodic_table = defaultdict(int)

# 存在しないキーに対する値は整数(int)の0になる

pefiodic_table["Hydrogen"] = 1
print(pefiodic_table["Lead"])

# defaultdictに存在しないキーを用いた場合、そのキーが自動で生成される
print(pefiodic_table)


def no_idea():
    return "Hun?"


# キーが存在しない場合に no_idea()が呼ばれる
bestiary = defaultdict(no_idea)

bestiary["A"] = "Atominable Snowman"
bestiary["B"] = "Basilisk"

print(bestiary["A"], bestiary["B"])

print(bestiary["C"])

# lambdaを使えば、これらの方の空のに値を返して存在しないキーのデフォルト値にすることができる。
test = defaultdict(lambda: "Huh?")

print(test["f"])

# intは独自カウンタを作るための手段になりうる
food_counter = defaultdict(int)

for food in ["spam", "spam", "egg", "spam"]:
    food_counter[food] += 1

for food, count in food_counter.items():
    print(food, count)

